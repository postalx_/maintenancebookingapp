import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet,Button, Text, View, Image } from 'react-native';


export default class Users extends React.Component {
    constructor() {
    super();
    this.state = {
        sampleText: true,
        realText: 'Login',
    };
    }

    changeTextValue = () => {
    this.setState({sampleText: !this.state.sampleText});
    if (this.state.sampleText){
        this.setState({realText: 'Logout'}); 
    } else {
        this.setState({realText: 'Login'}); 
    }
    }
    render(){
        return (
            <View style={styles.container}>
                <Image source={{uri: "https://cdn1.iconfinder.com/data/icons/website-internet/48/website_-_female_user-512.png"}}
        style={{width: 200, height: 200}}/>
            <Button onPress = {this.changeTextValue} title="Button"/>
            <Text >{this.state.realText}</Text>
            <Button
                    title="Cars"
                    onPress={() => this.props.navigation.navigate('Cars')}
            />
            <Button
                    title="Appointments"
                    onPress={() => this.props.navigation.navigate('Appointments')}
            />
            
            </View>
        )
    }      
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });