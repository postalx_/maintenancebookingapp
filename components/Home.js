import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';

import Users from "./Users";
import Cars from "./Cars";
import Appointments from "./Appointments";


const Home = createStackNavigator(
    {
        Cars: Cars,
        Appointments:Appointments,
        Users: Users,
        
    },
    {
        navigationOptions: {
            headerTintColor: '#fff',
            headerStyle: {
                backgroundColor: '#000',
            },
        },
    }
);

const container = createAppContainer(Home);

export default container;