import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet,Button, Text, View, Image,RefreshControl, SafeAreaView, ScrollView,FlatList, TextInput,Platform } from 'react-native';
// import { FlatList } from 'react-native-gesture-handler';

const isWeb = Platform.OS === 'web';

const wait = (timeout) => {
  return new Promise(resolve => setTimeout(resolve, timeout));
}


export default class Cars extends React.Component {
    

  // const onRefresh = React.useCallback(() => {
  //   setRefreshing(true);
  //   wait(2000).then(() => setRefreshing(false));
  // }, []);


  async componentDidMount() {
    console.log('component did mount');
    const response =  await fetchCars();
    // cars = JSON.parse(response[0].toString())
    console.log('response', response)
    // console.log('cars',cars)
    this.setState({ data: response })
    this.setState({ car_id: response[0].car_id })
    this.setState({ make: response[0].make })
    this.setState({ model: response[0].model })
    this.setState({ year: response[0].year })
    console.log(this.state.data)
  }
  
  constructor(props) {
        super(props);
        this.state = {
          refreshing:false,
          sampleText: 'Initial Text',
          data: [],
          car_id:'',
          make:'',
          model:'',
          odometer:'',
          year: ''
        };
      }
    
      changeTextValue = () => {
        this.setState({sampleText: this.props.name});
      }

      createNew = () =>{
        console.log(this.state.make, this.state.model)
        if (this.state.odometer === "") {
          alert('Please Enter a mileage');
          return;
        } else if (isNaN(this.state.year) || isNaN(this.state.odometer)) {
          alert('Please Enter a valid number');
          return;
        } 
        else {
          createCar(this.state.make,this.state.model,this.state.year,this.state.odometer);
        }
        
      }
      async forceUpdate(){
        console.log("update complete")
        const response =  await fetchCars();
        this.setState({ data: response })
      }

      onRefresh = () => {
        this.setState({refreshing: true});
        wait(2000).then(() =>{
        this.setState({refreshing: false})
        });
        console.log('Updating...')
        this.forceUpdate();
      }

    
      // _onPressButton() {
      //   <Text onPress = {this.changeTextValue}>
      //     {this.state.sampleText}
      //   </Text>
      // }
      render(){
      return (
        <View style={styles.container}>
          <ScrollView
          
        contentContainerStyle={styles.scrollView}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this.onRefresh}
          />
        }
      >
          <Image source={{uri: "https://png.pngtree.com/element_our/png_detail/20181229/vector-garage-icon-png_302706.jpg"}}
        style={{width: 200, height: 200}}/>
          <Button color={'#ae2'} onPress = {this.changeTextValue} title="Button"/>
          <Text >{this.state.sampleText}</Text>
          
          <Button
                    title="Users"
                    onPress={() => this.props.navigation.navigate('Users')}
            />
            <Button
                    title="Appointments"
                    onPress={() => this.props.navigation.navigate('Appointments')}
            />
         
          {/* <Text style={{alignItems:'left'}}>Car Year</Text>
          <Text style={{alignItems:'left'}}>Car Id: {this.state.car_id}</Text>
          <Text style={{alignItems:'left'}}>Make: {this.state.make}</Text>
          <Text style={{alignItems:'left'}}>Model: {this.state.model}</Text>
          <Text style={{alignItems:'left'}}>Year: {this.state.year}</Text> */}
          
          <Text style={{fontSize:30}}> List of Current Cars </Text>
          
          {this.state.data.map((car) => <Text key={car.car_id}>{car.make} {car.model} -  {car.year} with {car.odometer} miles on it. CarId({car.car_id})  </Text> )}
          
          <Text style={{fontSize:30}}> Create New Car </Text>
          <Text>Make</Text>
          <TextInput  
          onChangeText={(e) => this.setState({ make: e }) }
          style={styles.textinput} defaultValue={'Chrysler'} />
          <Text>Model</Text>
          <TextInput  
          onChangeText={(e) => this.setState({ model: e }) }
          style={styles.textinput} defaultValue={'Minivan'} />
          <Text>Year</Text>
          <TextInput  
          onChangeText={(e) => this.setState({ year: e }) }
          style={styles.textinput} defaultValue={'2020'} />
          <Text>Odometer</Text>
          <TextInput  
          onChangeText={(e) => this.setState({ odometer: e}) }
          
          style={styles.textinput} defaultValue={'00000'} />
          <Button color={'#ae2'} style={{marginBottom:15}} onPress = {this.createNew} title="Create New"/>
          <Text>Pull down to see RefreshControl indicator</Text>
          <Text>Pull down to see RefreshControl indicator</Text>
          <Text>Pull down to see RefreshControl indicator</Text>
          <Text>Pull down to see RefreshControl indicator</Text>
          <Text>Pull down to see RefreshControl indicator</Text>
          <Text>Pull down to see RefreshControl indicator</Text>
          </ScrollView>
        </View>
      )
    }
    
};


async function fetchCars() {
  // dispatch(companiesLoading(true));

  let headers = {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': '*',
    'Access-Control-Allow-Origin': 'http://localhost:3000/*',
    'Accept': "application/json",
    'admin': "true"
  }
  // const headers = 
  return fetch('http://localhost:3000/' + 'cars/all',{
    method: 'GET',
    // mode: 'cors',
    withCredentials: true,
    // mode: 'no-cors',
    headers: {
      "Content-Type": "application/json",
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': '*',
      'Access-Control-Allow-Origin': 'http://localhost:3000/*',
      'Accept': "application/json",
      'admin':'true'
    }
  })
      .then(response => {
          if (response.ok) {
              
              return response.json();
          }
          else {
              var error = new Error('Error ' + response.status + ': ' + response.statusText)
              error.response = response;
              throw error;
          }
      },
      error => {
          var errmess = new Error(error.message);
          throw errmess;
      })
      // .then(response => {
      //   response.json()
      // })
      // .then(companies => dispatch(addCompanies(companies)))
      // .catch(error => dispatch(companiesFailed(error.message)));
}

async function createCar(make1,model1,year1,odometer1) {
  // dispatch(companiesLoading(true));
  let reqBody = {
    "make":make1,
      "model":model1,
      "year":year1,
      "odometer":odometer1
  };

  console.log(JSON.stringify(reqBody));
 
  return fetch('http://localhost:3000/' + 'cars/create',{
    // mode: 'no-cors',
    method: 'POST',
    body: JSON.stringify(reqBody),
    headers: {
      "Content-Type": "application/json",
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': '*',
      'Access-Control-Allow-Origin': 'http://localhost:3000/*',
      'Accept': "application/json",
    }
    // headers: new Headers({
    //   // 'Authorization': 'Basic '+btoa('username:password'), 
    //   'Accept': "application/json",
    //   // 'Host':'localhost:3000',
    //   
      
    //   'Content-Type':'application/json'
    //   // 'admin': true,
    
    //   // headers.append('GET', 'POST', 'OPTIONS');
    // }), 
  })
      .then(response => {
          if (response.ok) {
            return response.json();
          }
          else {
              var error = new Error('Error ' + response.status + ': ' + response.statusText)
              error.response = response;
              throw error;
          }
      },
      error => {
          var errmess = new Error(error.message);
          throw errmess;
      })
      // .then(response => {
      //   response.json()
      // })
      // .then(companies => dispatch(addCompanies(companies)))
      // .catch(error => dispatch(companiesFailed(error.message)));
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
      height: 'auto',
    },
    scrollView: {
      flex: 1,
      backgroundColor: 'pink',
      alignItems: 'center',
      justifyContent: 'center',
    },
    textinput: {
      height: 26,
      borderWidth: 0.5,
      borderColor: '#0f0f0f',
      padding: 4,
      marginVertical: '1rem'
    },
    button:{
      width: 200,
      height: 100,
      color: '#aaa',
    }
  });
    


