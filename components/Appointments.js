import React, {Component} from 'react';
import { StyleSheet, Text, View, Dimensions, Button } from 'react-native';

const {width, height} = Dimensions.get('window');

export default class Appointments extends Component{
    render() {    
    return (
            <View  style={styles.header}>
                <Button
                    title="Cars"
                    onPress={() => this.props.navigation.navigate('Cars')}
            />
            <Button
                    title="Users"
                    onPress={() => this.props.navigation.navigate('Users')}
            />
            </View>
        )
    }
}
const styles = StyleSheet.create({
        header: {
            height: height/10,
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'center',
        },
});
