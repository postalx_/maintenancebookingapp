import { StatusBar } from 'expo-status-bar';
import {AppRegistry} from 'react-native';
import React from 'react';
import { RefreshControl,Dimensions, StyleSheet,SafeAreaView, ScrollView,Text, View} from 'react-native';
import Home from './components/Home'; 
import {expo as appName} from './app.json'; 


export default function App(){

  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    wait(2000).then(() => setRefreshing(false));
  }, []);

  return (
    <View style={styles.outer}>
      <Home/>
    </View>
    
  );
}
  
;

const styles = StyleSheet.create({
  outer: {
    flex: 1,

  },
  
  container: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

AppRegistry.registerComponent(appName.name, () => App);